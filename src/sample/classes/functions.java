package sample.classes;

import javafx.scene.control.Alert;


/**
 * function.java - Holds information for error methods across files
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */

public class functions {
    /**
     * Gives an alert if the user tries to enter no name for the album
     */
    public static void invalidAlbName(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Album Name Error!");
        alert.setHeaderText("Invalid Album Name");
        alert.setContentText("Please enter a valid Album Name");

        alert.showAndWait();
    }


    /**
     * Gives an alert if the user did not put a valid username
     */

    public static void invalidLogin() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Login Error!");
        alert.setHeaderText("Invalid Username");
        alert.setContentText("Incorrect Login, Try again! (For graders 'input admin for adminpage')");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the user did not enter a name for the album
     */
    public static void incorrectInput() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("No Input!");
        alert.setHeaderText("Invalid Name");
        alert.setContentText("No name provided.");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the album to be created has the same name as another album
     */

    public static void dupAlbum() {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Duplicate!");
        alert.setHeaderText("Album already Exists");
        alert.setContentText("Provide another name.");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the user cannot be deleted
     */

    public static void invalidUserDel(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Delete!");
        alert.setHeaderText("Cannot Delete user");
        alert.setContentText("Provide another user.");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the album cannot be deleted
     */
    public static void invalidAlbumDel(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Delete!");
        alert.setHeaderText("Cannot Delete album");
        alert.setContentText("Provide another album.");

        alert.showAndWait();

    }

    /**
     * Gives an alert if the photo cannot be opened
     */
    public static void invalidPhotoOpen(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid!");
        alert.setHeaderText("Cannot Open Photo");
        alert.setContentText("Create a photo before opening.");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the tag cannot be created
     */
    public static void invalidTagAdd(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Tag!");
        alert.setHeaderText("Cannot Add that tag");
        alert.setContentText("Provide another tag.");

        alert.showAndWait();
    }


    /**
     * Gives an alert if the cannot be deleted
     */

    public static void invalidTagDel(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Tag!");
        alert.setHeaderText("Cannot Delete tag");
        alert.setContentText("Provide another tag.");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the user has name no
     */

    public static void invalidUser(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Addition!");
        alert.setHeaderText("Cannot Add user with no name");
        alert.setContentText("Provide another user.");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the pasted image would be null
     */
    public static void invalidPaste(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Paste!");
        alert.setHeaderText("Cannot paste this image");
        alert.setContentText("Copy annother image");

        alert.showAndWait();
    }

    /**
     * Gives an alert if the the pasted image would be a duplicate in the album
     */

    public static void dupPhoto(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Duplicate Photo!");
        alert.setHeaderText("Cannot paste this image");
        alert.setContentText("Copy another image");

        alert.showAndWait();
    }


    /**
     * Gives an alert if the the pasted image would be a duplicate in the album
     */

    public static void dupPhotos(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Duplicate Photo!");
        alert.setHeaderText("There were duplicate images");
        alert.setContentText("");

        alert.showAndWait();
    }


    /**
     * Gives an alert if the user attempts to delete an invalid photo
     */

    public static void invalidPhotoDel(){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Invalid Photo Deletion!");
        alert.setHeaderText("Cannot delete this image");
        alert.setContentText("Delete another image");

        alert.showAndWait();
    }
}

