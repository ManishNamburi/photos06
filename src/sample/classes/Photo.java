package sample.classes;

import javafx.scene.image.Image;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;


/**
 * Photo.java - Holds information for Photo class
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */


public class Photo implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String caption;
    private String name;
    private Date date;
    private ArrayList<Tag> tags = new ArrayList<>();
    private Image image;

    private final String location;

    /**
     * @param image    the image variable of the selected photo
     * @param location the location of the image
     */
    public Photo(Image image, String location) {
        this.location = location;
        this.image = image;
    }

    public Photo(String location) {
        this.location = location;
    }

    public Photo(String location, Date date) {
        this.location = location;
        this.date = date;
    }

    public Photo(String location, String caption) {
        this.location = location;
        this.caption = caption;
    }

    public Photo(String location, Date date, String caption) {
        this.location = location;
        this.date = date;
        this.caption = caption;
    }


    /**
     * This method returns the name of a photo
     *
     * @return name        String name of the of photo
     */

    public String getName() {
        return name;
    }

    /**
     * This method returns the date string of a photo
     * @return String date of photo
     */
    public String getStringDate() {
        return new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(getDate());
    }

    public ArrayList<String> stringTags() {
        ArrayList<String> tagList = new ArrayList<String>();
        if (tags == null) {
            return tagList;
        }
        for (Tag t : tags) {
            tagList.add(t.toString());
        }
        return tagList;
    }


    /**
     * This method sets the name of a photo
     *
     * @param name String name of the of photo
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method returns the date of a photo
     *
     * @return this.date        Date date of the of photo
     */

    public Date getDate() {
        return this.date;
    }

    /**
     * This method sets the date of a photo
     *
     * @param date Date the of photo
     */

    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * This method returns a specific tag
     * @param i     The location of the tag you want
     * @return      ArrayList<Tag> The tag requested at the location
     */
    public Tag getTagAt(int i) {
        return this.tags.get(i);
    }

    /**
     * This method gets the tags for a photo
     * @return ArrayList<Tags> The tags for a photo
     */
    public ArrayList<Tag> getTags() {
        return this.tags;
    }

    /**
     * This method sets the tags of a photo
     *
     * @param tags Array list of tags
     */

    public void setTags(ArrayList<Tag> tags) {
        this.tags = tags;
    }

    /**
     * Adds tags inputted by user to photo
     *
     * @param name  the name of tag
     * @param value the value of tag
     */

    public Tag addTag(String name, String value) {

        Tag newtag = new Tag();

        for (Tag tag : this.tags) {
            if (tag.getName().equals(name) && tag.getValue().equals(value)) {
                functions.invalidTagAdd();
                return newtag;
            }
        }
        newtag = new Tag(name, value);
        this.tags.add(newtag);
        return newtag;
    }

    /**
     * This method deletes the tag entered
     *
     * @param value String the tag being search for
     * @return true if the tag is found false if not
     */

    // For loop, goes through tags if it is equal to the entry data removes it
    public boolean deleteTag(Tag value) {
        this.tags.remove(value);
        return false;
    }

    /**
     * This method searches to see if the tag is present
     *
     * @param value String the tag being search for
     * @return true if the tag is found false if not
     */

    public boolean searchTag(String value) {
        for (int i = 0; i < this.tags.size(); i++) {
            if (this.tags.get(i).toString().contains(value)) {
                return true;
            }
        }
        return false;
    }


    public boolean searchTag(String name, String value){
        for (int i = 0; i < this.tags.size(); i++) {
            if (this.tags.get(i).toString().contains(value) && this.tags.get(i).toString().contains(name) ) {
                return true;
            }
        }
        return false;
    }


    /**
     * This method returns the size of the photos
     *
     * @return this.caption      String the caption of the photo
     */

    public String getCaption() {
        return this.caption;
    }


    public String getLocation() {
        return this.location;
    }

    /**
     * This method sets the size of the photos
     *
     * @param caption String the caption of the photo
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", tags=" + tags +
                '}';
    }
}
