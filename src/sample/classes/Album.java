package sample.classes;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;


/**
 * Album.java - Holds information for Album class
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */

public class Album implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String name;
    private Integer size;
    private String range;

    public final ArrayList<Photo> photos;

    public Album() {
        this.photos = new ArrayList<Photo>();
        this.name = "test Album";
        //this.size = photos.size();
    }

    /**
     * This method returns the size of the photos
     * @return  this.photos.size	    the size of the photos
     */
    public Integer getSizeValue() {
        return this.photos.size();
    }

    /**
     * This method returns the photos from the array list
     * @return  this.photos	    the photos
     */

    public ArrayList<Photo> getPhotos() {
        return this.photos;
    }

    /**
     * This method adds a photo to the Album
     * @param   photo           location of photo
     */

    public void addPhoto(Photo photo) {
        this.photos.add(photo);
    }

    /**
     * This method removes a photo from the Album
     * @param   photo           the photo
     */

    public void removePhoto(Photo photo) {
        this.photos.remove(photo);
    }

    /**
     * This method gets the latest date from the Album
     * @return   curr.getDate           the latest date for the album
     */

    public String oldestDate() {
        if (this.photos.size() < 1) {
            return null;
        }
        Photo curr = this.photos.get(0);
        for (Photo photo : photos) {
            if(curr.getDate().compareTo(photo.getDate()) > 0){
                curr = photo;
            }
        }
        return curr.getStringDate();
    }

    /**
     * This method gets the oldest date from the Album
     * @return   curr.getDate           the oldest date for the album
     */

    public String latestDate() {
        if (this.photos.size() < 1) {
            return null;
        }
        Photo curr = this.photos.get(0);
        for (Photo photo : photos) {
            if(curr.getDate().compareTo(photo.getDate()) < 0){
                curr = photo;
            }
        }
        return curr.getStringDate();
    }

    /**
     * This method gets the name of the Album
     * @return   this.name        string name of the album
     */

    public String getName() {
        return this.name;
    }

    /**
     * This method sets the name of the Album
     * @param name        string name of the album
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method gets the size of the Album
     * @return   this.size       integer size of the album
     */

    public Integer getSize() {
        return this.size;
    }

    /**
     * This method sets the size of the Album
     * @param   size       integer size of the album
     */

    public void setSize(Integer size) {
        this.size = size;
    }






    /**
     * This method gets the Range of the Album
     * @return   this.range       String range of the album
     */

    public String getRange() {
        return this.range;
    }

    /**
     * This method sets the Range of the Album
     * @param    range       String range of the album
     */

    public void setRange(String range) {
        this.range = range;
    }

    @Override
    public String toString() {
        return "Name = "+ name + "\tSize = " + this.photos.size() +
                " photo(s)" + "\tRange = " +
        oldestDate() + " - " + latestDate();


    }

}
