package sample.classes;

import java.io.*;
import java.util.ArrayList;

/**
 * User.java - Holds information for User class
 */

public class User implements Serializable{

    @Serial
    private static final long serialVersionUID = 1L;

    private String username;

    public ArrayList<Album> albums;


    //albums array

    public User(String username){
        this.username = username;
        this.albums = new ArrayList<Album>();
    }

    /**
     * This method gets the username of the user
     * @return    username	  String the username
     */

    public String getUsername() {
        return username;
    }

    /**
     * This method sets the username of the user
     * @param     username	  String the username
     */

    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * This method gets the Albums of the user
     * @return    this.albums	  ArrayList<Album> the albums of the user
     */

    public ArrayList<Album> getAlbums() {
        return this.albums;
    }

    /**
     * This method sets the albums of the user
     * @param     albums	  ArrayList the albums of the user
     */

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

    /**
     * This method creates a new album prints the list of albums with the new addition
     * @param     name	  String the name the user wants for the album
     */

    public Album newAlbum(String name){
        Album newAlbum = new Album();
        newAlbum.setName(name);
        this.albums.add(newAlbum);
        System.out.println("current Albums: " + this.albums);
        return newAlbum;
    }

    /**
     * This methods removes an album
     * @param name          String the name of the album to be removed
     * @return              Boolean true if deleted from albums
     */
    public boolean removeAlbum(String name){
        for (Album album : this.albums ) {
            if(name.equalsIgnoreCase(album.getName())){
                this.albums.remove(album);
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "username='" + username + '\'';
    }


}
