package sample.classes;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;


/**
 * Admin.java - Holds information for Admin class
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */

public class Admin {

    public static final String fileName = "admin.dat";
    public static final String dir = "src/sample/dat/";

    private static ArrayList<User> users = new ArrayList<User>(Arrays.asList(new User("stock")));

    /**
     * This method saves the list of users
     * @throws IOException
     */

    public static void save() throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(dir + File.separator + fileName));
        oos.writeObject(users);
    }

    /**
     * This method loads the list of users
     * @throws IOException
     * @throws ClassNotFoundException
     */

    public static void load() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(dir + File.separator + fileName));
        users = (ArrayList) ois.readObject();
    }

    /**
     * This method adds a user if the username isn't taken
     * @param username        the string username of the new user
     * @return boolean
     */

    public static boolean addUser(String username) {
        for (User user : users) {
            if (user.getUsername().equals(username)) { // check dupllicate
                return false;
            }
        }

        users.add(new User(username));
        return true;
    }

    public static void addUser(User user){
        users.add(user);
    }

    /**
     * This method deletes a user
     * @param username       the username of the user to be deleted
     * @return               Boolean
     */
    public static boolean deleteUser(String username) {
        for (int i = 0; i < users.size(); i++) {
            if (username.equals(users.get(i).getUsername())) {
                users.remove(i);
                return true;
            }
        }
        return false;
    }

    public static void deleteUser(User user) {
        users.remove(user);
    }


    /**
     * The method returns all usernames
     * @return usernames        the usernames of all users
     */
    public static ArrayList<String> allUsernames() {
        ArrayList<String> usernames = new ArrayList<String>();

        for (User user : users) {
            usernames.add(user.getUsername());
        }
        return usernames;
    }

    /**
     * This method finds the user from the username
     * @param  username
     * @return  user   Returns the user
     */

    public static User login(String username) {
        for (User user : users) {
            if (username.equals(user.getUsername())) {
                return user;
            }
        }
        return null;
    }

}
