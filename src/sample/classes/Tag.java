package sample.classes;

import java.io.Serial;
import java.io.Serializable;

/**
 * Tag.java - Holds information for Tag class
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */

public class Tag implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private String name;
    private String value;


    public Tag(String name, String value){
        this.name = name;
        this.value = value;
    }

    public Tag(){}

    /**
     * This method gets the name of the tag
     * @return    name	  String the value of the tag
     */

    public String getName() {
        return name;
    }

    /**
     * This method sets the name of the tag
     * @param   name	  String the value of the tag
     */

    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method gets the value of the tag
     * @return    value	  String the value of the tag
     */

    public String getValue() {
        return value;
    }

    /**
     * This method sets the value of the tag
     * @param   value	  String the value of the tag
     */

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return name + ", " + value;
    }
}
