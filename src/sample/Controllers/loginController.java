package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.classes.Admin;
import sample.classes.User;
import sample.classes.functions;

import java.io.IOException;

public class loginController {

    @FXML
    private TextField username;

    /**
     * This method logs in a user
     * @param actionEvent           press the button
     * @throws IOException
     */

    public void loginClick(javafx.event.ActionEvent actionEvent) throws IOException {
        String usernameValue = username.getText();

        User user = Admin.login(usernameValue);


        if (user == null && !usernameValue.equals("admin")) {
            functions.invalidLogin();
        }

        //if admin then load adminpage
        if (usernameValue.equals("admin")) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/admin.fxml"));
            Parent parent = loader.load();

            //adminPage controller = loader.<adminPage>getController();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            adminController controller = loader.getController();
            controller.start();
            stage.setScene(scene);
            stage.show();
        } else if (user != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/user.fxml"));
            Parent parent = loader.load();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            userController controller = loader.getController();
            controller.start(user);
            stage.setScene(scene);
            stage.show();
        }


    }
}
