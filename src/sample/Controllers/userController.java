package sample.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import sample.classes.Album;
import sample.classes.Photo;
import sample.classes.User;
import sample.classes.functions;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.IOException;
import java.util.*;

/**
 * userController.java - Holds information for user.fxml
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */


public class userController {
    @FXML
    private ListView<Album> listView;

    @FXML
    private Label usernameLabel;

    @FXML
    private TextField albumName;



    private User currentUser;

    private ArrayList<Album> userAlbums;

    /**
     * This method starts the User and fetches all the data needed
     *
     * @param user User The user inputted
     */

    public void start(User user) {


        currentUser = user;
        usernameLabel.setText(currentUser.getUsername());
        userAlbums = currentUser.getAlbums();


        ObservableList<String> obsList;
        obsList = FXCollections.observableArrayList(userAlbums.toString());


        listView.setItems(FXCollections.observableArrayList(userAlbums));


        listView.getSelectionModel().select(0);

    }


//    private void populateDirectoryListView() {
//        ObservableList<String> obsList;
//        ArrayList<String> values = new ArrayList<String>();
//        values.add(currentUser.getUsername());
//        for (Album a : userAlbums) {
//            values.add('\t' + a.getName());
//        }
//
//        obsList = FXCollections.observableArrayList(values);
//        leftListView.setItems(obsList);
//    }


    /**
     * This method adds an album
     *
     * @return Returns the new album
     */
    public void addAlbum() {
        TextInputDialog dialog = new TextInputDialog("Album Name");
        dialog.setTitle("New Album");
        dialog.setHeaderText("Please enter the name for your new album. No duplicates.");
        dialog.setContentText("Album Name:");


        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {

            if(result.get().equals("") || result.get().contains("/")  || result.get().contains("/") || result.get().contains(" ")){
                functions.invalidAlbName();
                return;
            }

            String albumName = result.get();

            for (Album album : userAlbums) {
                if (album.getName().equalsIgnoreCase(albumName)) {
                    functions.dupAlbum();
                    return;
                }
            }

            currentUser.newAlbum(albumName);
            userAlbums = currentUser.getAlbums();
            listView.setItems(FXCollections.observableArrayList(userAlbums));

            listView.getSelectionModel().select(userAlbums.size()-1);
            listView.refresh();
        } else {
            functions.incorrectInput();
            return;
        }
    }

    /**
     * This method removes the selected album on button press after confirmation
     */
    public void removeAlbum() {
        Album albumDel = listView.getSelectionModel().getSelectedItem();

        if(albumDel == null){
            functions.invalidAlbumDel();
            return;
        }

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Delete");
        alert.setHeaderText("You are about to delete " + albumDel);
        alert.setContentText("Are you ok with this?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            int index = userAlbums.indexOf(albumDel);
            currentUser.removeAlbum(albumDel.getName());
            userAlbums = currentUser.getAlbums();
            listView.setItems(FXCollections.observableArrayList(userAlbums));
            listView.getSelectionModel().select(index-1);
            listView.refresh();
        }
    }

    /**
     * This method renames the album to the name the user inputs
     */
    public void renameAlbum(){
        Album currAlbum = listView.getSelectionModel().getSelectedItem();

        if(listView.getSelectionModel().getSelectedIndex() < 0){
            return;
        }

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Rename Album");
        dialog.setHeaderText("New Album Name: ");
        dialog.showAndWait();

        if(dialog.getEditor().getText().equals(null)) { }
        else{
            currAlbum.setName(dialog.getEditor().getText());
            userAlbums = currentUser.getAlbums();
            listView.setItems(FXCollections.observableArrayList(userAlbums));
        }

    }

    /**
     * This method opens an album and takes the user to the opened album page
     * @param actionEvent                On button click preforms open
     * @throws IOException               Throws IOException if error
     */
    public void openAlbum(javafx.event.ActionEvent actionEvent) throws IOException {

        Album currAlbum = listView.getSelectionModel().getSelectedItem();

        if(listView.getSelectionModel().getSelectedIndex() < 0){
            return;
        }

        currAlbum.getSize();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/album.fxml"));
        Parent parent = loader.load();

        //adminPage controller = loader.<adminPage>getController();
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        albumController controller1 = loader.getController();
        controller1.start(currAlbum, currentUser);
        stage.setScene(scene);
        stage.show();

    }



    public void cancel(){
        albumName.clear();
    }


    /**
     * This method searches for a photo
     * @param actionEvent   ActionEvent on button click
     * @throws IOException  Throws IOException
     * @throws ParseException   Throws ParseException
     */
    public void search(javafx.event.ActionEvent actionEvent) throws IOException, ParseException {

        if(albumName.getText() ==null) return;

        if(albumName.getText().equals(""))return;
        if(albumName.getText().equals(""))return;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Search");
        alert.setHeaderText("Match search values properly.\n Search by date needs follow dd-M-yyyy hh:mm, dd-M-yyyy hh:mm\n Search by tag needs to follow tag=value OR tag=value depending on Conjunctive or Disjunctive and how many values.");
        alert.setContentText("Choose your option.");

        ButtonType buttonTypeOne = new ButtonType("Search By Date");
        ButtonType buttonTypeTwo = new ButtonType("Search By Tags");
        ButtonType buttonTypeCancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

        alert.getButtonTypes().setAll(buttonTypeOne, buttonTypeTwo, buttonTypeCancel);

        if(listView.getSelectionModel().getSelectedIndex() < 0){
            return;
        }

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == buttonTypeOne){
            // ... user chose "One"
            searchByDate(albumName.getText());

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/search.fxml"));
            Parent parent = loader.load();

            //adminPage controller = loader.<adminPage>getController();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            searchController controller = loader.getController();
            controller.start(searchByDate(albumName.getText()), currentUser, userAlbums.get(listView.getSelectionModel().getSelectedIndex()));
            stage.setScene(scene);
            stage.show();


        } else if (result.get() == buttonTypeTwo) {
            // ... user chose "Two"
            searchByTags(albumName.getText());

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/search.fxml"));
            Parent parent = loader.load();

            //adminPage controller = loader.<adminPage>getController();
            Scene scene = new Scene(parent);
            Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
            searchController controller = loader.getController();
            controller.start(searchByTags(albumName.getText()), currentUser, userAlbums.get(listView.getSelectionModel().getSelectedIndex()));
            stage.setScene(scene);
            stage.show();



        }else {
            // ... user chose CANCEL or closed the dialog
            return;
        }

    }

    /**
     * This method searches for a photo by date
     * @param value         String The date value entered by the user
     * @return              ArrayList<Photo> result the results of the search
     * @throws ParseException       Throws ParseException
     */
    private ArrayList<Photo> searchByDate(String value) throws ParseException {
        if(currentUser.albums.size() < 1 ) return null;
        ArrayList<Photo> result = new ArrayList<>();
        List<String> parts = new ArrayList<String>();

        parts = Arrays.asList(value.trim().split("\\s*,\\s*"));

        if(parts.size() < 2){
            return null;
        }

        String date1 = parts.get(0);
        String date2 = parts.get(1);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm");

        Date dateOne = sdf.parse(date1);
        Date dateTwo = sdf.parse(date2);

        dateTwo.setSeconds(59);

        System.out.println(dateOne + " "+ dateTwo);

       for (Album album : currentUser.albums){
           for (Photo photo : album.photos) {

               if( (photo.getDate().equals(dateOne) || photo.getDate().equals(dateTwo)) ||
                       (photo.getDate().after(dateOne) && photo.getDate().before(dateTwo) )){
                   result.add(photo);
               }
           }
           System.out.println(result);
       }



        return result;
    }


    // person=vishnu AND person=manish

    /**
     * This method searches by Tags
     * @param value         String the tag the user wants to search by
     * @return              ArrayList<Photo> results the results of the search
     */
    private ArrayList<Photo> searchByTags(String value){
        if(currentUser.albums.size() < 1 ) return null;

        int andBoolean = 0;

        List<String> parts = new ArrayList<String>();
        if(value.contains("OR")){
            parts = Arrays.asList(value.trim().split("\\s*OR\\s*"));
            andBoolean = 0;
        }else if (value.contains("AND")){
            parts = Arrays.asList(value.trim().split("\\s*AND\\s*"));
            andBoolean = 1;
        }else{
            parts.add(value);
        }

        System.out.println(parts);
        ArrayList<Photo> results = new ArrayList<>();

        List<String> firstPart = new ArrayList<String>();
        List<String> secondPart = new ArrayList<String>();
        String firstValue = "";
        String secondValue = "";

        String firstName = "";
        String secondName = "";

        if(parts.size() > 0){
            firstPart = Arrays.asList(parts.get(0).trim().split("\\s*=\\s*"));
            if(firstPart.size() < 2){
                return null;
            }
            firstName = firstPart.get(0);
            firstValue = firstPart.get(1);
            if(parts.size() > 1){

                secondPart = Arrays.asList(parts.get(1).trim().split("\\s*=\\s*"));

                if(secondPart.size() < 2){
                    return null;
                }

                secondName = secondPart.get(0);
                secondValue = secondPart.get(1);
            }
        }



        if(secondPart.size() < 1){
            for (Album album : currentUser.albums) {
                for (Photo photo : album.photos) {
                    if (photo.searchTag(firstName.toUpperCase(), firstValue)){
                        results.add(photo);
                    }
                }
            }
        }else{
            if (andBoolean == 1){
                for (Album album : currentUser.albums) {
                    for (Photo photo : album.photos) {
                        if (photo.searchTag(firstName.toUpperCase(), firstValue) && photo.searchTag(secondName.toUpperCase(), secondValue)) {
                            results.add(photo);
                        }
                    }
                }
            }else{
                for (Album album : currentUser.albums) {
                    for (Photo photo : album.photos) {
                        if (photo.searchTag(firstName.toUpperCase(), firstValue) || photo.searchTag(secondName.toUpperCase(), secondValue)) {
                            results.add(photo);
                        }
                    }
                }
            }
        }
        System.out.println(results);
        return results;
    }


    /**
     * This method logsout a User
     *
     * @param actionEvent Button click to log out
     * @throws IOException
     */

    public void logoutUser(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/Login.fxml"));
        Parent parent = loader.load();

        //adminPage controller = loader.<adminPage>getController();
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        loginController controller = loader.getController();
        stage.setScene(scene);
        stage.show();
    }
}
