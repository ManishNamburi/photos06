package sample.Controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import sample.classes.Album;
import sample.classes.Photo;
import sample.classes.Tag;
import sample.classes.User;
import sample.classes.functions;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;


/**
 * albumController.java - Holds information for album.fxml
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */


public class albumController {

    @FXML
    ListView<ImageView> listView;

    @FXML
    Button addPhoto;

    @FXML
    Label albumName, albumDate, photoCaption;

    @FXML
    ListView<String> tagsList;

    @FXML
    TextField searchBar;


    private static Stage mainStage;


    private static final ObservableList<ImageView> obs = FXCollections.observableArrayList();
    public static Album album;
    public User currUser;

    public static Photo copy;
    public static Photo currPhoto;


    //private static final ObservableList<Tag> tagsObs = FXCollections.observableArrayList();

    /**
     * This method starts the controller with the current album and user
     *
     * @param currAlbum Album the album that is currently selected
     * @param user      User the user that is logged in
     * @throws IOException Throws IOException
     */
    public void start(Album currAlbum, User user) throws IOException {


        album = currAlbum;
        currUser = user;

        albumName.setText(currAlbum.getName());

        obs.clear();

        mainStage = (Stage) addPhoto.getScene().getWindow();


        int size = album.photos.size();

        for (int i = 0; i < size; i++) {
            File path = new File(album.photos.get(i).getLocation());
            if (path != null) {
                Photo picture = new Photo(path.toString());
                Image image = new Image(path.toURI().toString(), 100, 100, false, false);
                obs.add(new ImageView(image));
            }
        }

        listView.setItems(obs);

        listView.getSelectionModel().select(0);

//        if(listView.getSelectionModel().getSelectedIndex() > -1){
//            listView.getSelectionModel().getSelectedIndex()
//            photoCaption.setText();
//
//        }
        int index = listView.getSelectionModel().getSelectedIndex();
        System.out.println("SIZE "+size);

        if (size == 0) {
            photoCaption.setText("[None]");
            albumDate.setText("[None]");
        } else {
            updateTags();
            refreshView();


            if (index > -1) {
                if (album.photos.get(index).getCaption() != null) {
                    if (album.photos.get(index).getCaption().equals("")) {
                        String caption = "[None]";
                        photoCaption.setText(caption);
                    }
                } else {
                    String caption = "[None]";
                    photoCaption.setText(caption);
                }
            }
            albumDate.setText(album.photos.get(index).getStringDate());
            //albumDate.setText();
        }
    }

    /**
     * This method updates the tags of an album
     */
    public void updateTags() {
        int index = listView.getSelectionModel().getSelectedIndex();
        ArrayList<String> tags = album.photos.get(index).stringTags();
        System.out.println("tags " + tags);
        tagsList.setItems(FXCollections.observableArrayList(tags));
        tagsList.getSelectionModel().select(0);
    }

    /**
     * This method adds a photo to an album
     *
     * @throws ParseException Throws a ParseException
     */
    public void add() throws ParseException {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select a photo");
        File path = fileChooser.showOpenDialog(mainStage);
        System.out.println(path);
        if (path != null) {
            Date d = new Date(path.lastModified());


            Photo picture = new Photo(path.toString(), d);

            if (dupCheck(picture)) {
                functions.dupPhoto();
                return;
            }

            if(addCheck(picture) != null){
                picture = addCheck(picture);

            }

            Image image = new Image(path.toURI().toString(), 100, 100, false, false);
            obs.add(new ImageView(image));
            album.photos.add(picture);
        }
        listView.getSelectionModel().select(album.photos.size() - 1);
        albumDate.setText(album.photos.get(listView.getSelectionModel().getSelectedIndex()).getStringDate());
        updateTags();
    }

    /**
     * This method refreshes the view
     */
    private void refreshView() {
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ImageView>() {
            public void changed(ObservableValue<? extends ImageView> observable,
                                ImageView oldValue, ImageView newValue) {

                int i = listView.getSelectionModel().getSelectedIndex();
                if (i > -1) {
                    currPhoto = album.photos.get(i);
                } else {
                    photoCaption.setText("[None]");
                    albumDate.setText("[None]");
                    tagsList.setItems(FXCollections.observableArrayList(new ArrayList<String>()));
                    return;
                }

                System.out.println(currPhoto.stringTags());
                if (i > -1) {
                    albumDate.setText(album.photos.get(i).getStringDate());

                    if (album.photos.get(i).getCaption() == null) {
                        photoCaption.setText("[None]");
                    } else {
                        photoCaption.setText(album.photos.get(i).getCaption());
                    }
                    Photo selPhoto = album.photos.get(i);
                }
                System.out.println(i);
                updateTags();
            }
        });
    }


    /**
     * Checks to see if there are no duplicates inside of a specific album
     *
     * @param photo photo to check
     * @return true if there is duplicate false if there is no duplciates
     */
    private boolean dupCheck(Photo photo) {

        for (Photo p : album.photos) {
            if (p.getLocation().equals(photo.getLocation())) {
                return true;
            }
        }
        return false;
    }

    private Photo addCheck(Photo photo) {
        Photo newPhoto = null;

        for (Album a : currUser.albums) {
            for (Photo p : a.photos) {
                if(p.getLocation().equals(photo.getLocation())){
                    newPhoto = p;
                }
            }
        }
        return newPhoto;
    }


    /**
     * This method deletes a photo from an album
     */
    public void delete() {
        int index = listView.getSelectionModel().getSelectedIndex();

        if (index < 0) {
            functions.invalidPhotoDel();
            return;
        }

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Delete");
        alert.setHeaderText("Are You Sure You Wish to Delete This Image?");
        Optional<ButtonType> con = alert.showAndWait();

        if (con.get() == ButtonType.OK) {
            album.photos.remove(index);
            obs.remove(index);
        }
    }

    /**
     * This method copies a photo
     */
    public void copy() {
        int i = listView.getSelectionModel().getSelectedIndex();
        copy = album.photos.get(i);
        System.out.println("Copy");
    }

    /**
     * This method pastes a photo
     */
    public void paste() {
        System.out.println(copy);
        if (copy != null) {
            if (dupCheck(copy)) {
                functions.dupPhoto();
                return;
            }

            album.photos.add(copy);
            Image image = new Image((new File(copy.getLocation())).toURI().toString(), 100, 100, false, false);
            obs.add(new ImageView(image));
            System.out.println("Paste");
        } else {
            functions.invalidPaste();
        }
    }

    /**
     * This method moves a photo
     */
    public void move() {
        int i = listView.getSelectionModel().getSelectedIndex();
        copy = album.photos.get(i);

        int index = listView.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Move");
        alert.setHeaderText("Are You Sure You Wish to Move This Image?\nSelected image will be replaced and pasted on okay \n (If you do not paste you will lose data)");
        Optional<ButtonType> con = alert.showAndWait();

        if (con.get() == ButtonType.OK) {
            album.photos.remove(index);
            obs.remove(index);
        }
    }

    /**
     * This method opens an image in the individual photo viewer
     *
     * @param actionEvent ActionEvent the user pressing a button
     * @throws IOException Throws IOException
     */
    public void openImage(javafx.event.ActionEvent actionEvent) throws IOException {

        int index = listView.getSelectionModel().getSelectedIndex();

        if (index < 0) {
            functions.invalidPhotoOpen();
            return;
        }

        Photo currImage = album.photos.get(index);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/Individual_Photoview.fxml"));
        Parent parent = loader.load();

        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        photoController controller1 = loader.getController();

        controller1.start(currImage, album, currUser);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * This method edits a caption for a photo
     */
    public void editCaption() {

        int selectedIndex = listView.getSelectionModel().getSelectedIndex();

        Photo photo = album.photos.get(selectedIndex);

        TextInputDialog dialog = new TextInputDialog(photo.getCaption());
        dialog.setTitle("Edit Caption");
        dialog.setHeaderText("Please enter a caption for your album");
        dialog.setContentText("Album Caption:");


        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            String caption = result.get();
            photo.setCaption(result.get());
            System.out.println(photo.getCaption());
            photoCaption.setText(caption);
        }

    }

    /**
     * This method adds a Tag to a photo
     */
    public void addTag() {

        int selectedIndex = listView.getSelectionModel().getSelectedIndex();

        if (selectedIndex < 0) {
            functions.invalidTagDel();
            return;
        }

        Photo photo = album.photos.get(selectedIndex);

        List<String> choices = new ArrayList<>();
        choices.add("a");
        choices.add("b");
        choices.add("c");

        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add Tag");
        dialog.setHeaderText("Type of Tag: ");
        dialog.showAndWait();
        String type = dialog.getEditor().getText();
        type = type.toUpperCase();

        TextInputDialog dialog2 = new TextInputDialog();
        dialog2.setTitle("Add Tag");
        dialog2.setHeaderText("Tag Content: ");

        dialog2.showAndWait();
        String data = dialog2.getEditor().getText();

        if (type.equals("")) {
            functions.invalidTagAdd();
            return;
        }
        if (data.equals("")) {
            functions.invalidTagAdd();
            return;
        }

        photo.addTag(type, data);

        updateTags();

        System.out.println(photo.getTags());
        tagsList.getSelectionModel().select(photo.stringTags().size() - 1);
    }

    /**
     * This method removes a tag from a photo
     */
    public void removeTag() {
        int photoIndex = listView.getSelectionModel().getSelectedIndex();
        int tagIndex = tagsList.getSelectionModel().getSelectedIndex();

        if (photoIndex < 0 || tagIndex < 0) {
            functions.invalidTagDel();
            return;
        }

        Photo photo = album.photos.get(photoIndex);
        Tag tag = photo.getTagAt(tagIndex);


        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm Delete");
        alert.setHeaderText("Are You Sure You Wish to Delete This tag?");
        Optional<ButtonType> con = alert.showAndWait();

        if (con.get() == ButtonType.OK) {
            photo.deleteTag(tag);
        } else {
            return;
        }
        updateTags();
        tagsList.getSelectionModel().select(photo.stringTags().size() - 1);
    }


    public void cancel() {
        searchBar.clear();
    }

    /**
     * This method logs a user out and sends them back to the log in screen
     *
     * @param actionEvent ActionEvent a button press
     * @throws IOException Throws IOException
     */
    public void logoutUser(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/Login.fxml"));
        Parent parent = loader.load();

        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        loginController controller = loader.getController();
        stage.setScene(scene);
        stage.show();
    }

    /**
     * This method takes a user back to the user intro page
     *
     * @param actionEvent ActionEvent user button press
     * @throws IOException Throws IOException
     */
    public void back(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/user.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        userController controller = loader.getController();
        controller.start(currUser);
        stage.setScene(scene);
        stage.show();
    }


}
