package sample.Controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.classes.Album;
import sample.classes.Photo;
import sample.classes.User;
import sample.classes.functions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

/**
 * searchController.java - Holds information for search.fxml
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */


public class searchController {

    @FXML
    ListView<ImageView> listView;

    @FXML
    Label caption, date;

    @FXML
    ListView<String> tagsList;


    public static Album currAlbum;
    private static Photo currPhoto;
    private static User currUser;
    private static final ObservableList<ImageView> obs = FXCollections.observableArrayList();

    private static Photo currPhoto1;

    ArrayList<Photo> results1 = new ArrayList<>();
    ArrayList<Photo> tempresults = new ArrayList<>();

    /**
     * This method starts the search page
     * @param results       ArrayList<Photo> the results of the search
     * @param user          User the current user
     * @param album         Album the album the user is in
     */
    public void start(ArrayList<Photo> results, User user, Album album) {

        obs.clear();

        currUser = user;
        results1 = results;
        currAlbum = album;
        tempresults = results;

        System.out.println(results);


        if (results != null) {
            if (results.size() == 0) {
                //empty results
                return;
            }
        } else {
            return;
        }


        if (results.size() == 0) {
            caption.setText("[None]");
            date.setText("[None]");
            return;
        }

        int size = results.size();

        for (int i = 0; i < size; i++) {
            File path = new File(results.get(i).getLocation());
            if (path != null) {
                Photo picture = new Photo(path.toString());
                Image image = new Image(path.toURI().toString(), 100, 100, false, false);
                obs.add(new ImageView(image));
            }
        }

        listView.setItems(obs);
        listView.getSelectionModel().select(0);

        if (listView.getSelectionModel().getSelectedItem() != null) {
            int index = listView.getSelectionModel().getSelectedIndex();
            caption.setText(results.get(index).getCaption());
            date.setText(results.get(index).getStringDate());
            updateTags();
            refreshView();
        }

    }

    /**
     * This method refreshes the view of the page
     */
    private void refreshView() {
        listView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<ImageView>() {
            public void changed(ObservableValue<? extends ImageView> observable,
                                ImageView oldValue, ImageView newValue) {

                results1 = tempresults;

                int i = listView.getSelectionModel().getSelectedIndex();
                if (i > results1.size()) {
                    return;
                }

                if (i > -1) {
                    currPhoto = results1.get(i);
                } else {
                    caption.setText("[None]");
                    date.setText("[None]");
                    tagsList.setItems(FXCollections.observableArrayList(new ArrayList<String>()));
                    return;
                }

                System.out.println(currPhoto.stringTags());
                if (i > -1) {
                    date.setText(currPhoto.getStringDate());

                    if (currPhoto.getCaption() == null) {
                        caption.setText("[None]");
                    } else {
                        caption.setText(currPhoto.getCaption());
                    }
                    //Photo selPhoto = album.photos.get(i);
                }
                System.out.println(i);
                updateTags();
            }
        });
    }

    /**
     * This method updates the tags on the page
     */
    public void updateTags() {
        int index = listView.getSelectionModel().getSelectedIndex();

        if (index < 0) {
            return;
        }

        ArrayList<String> tags = results1.get(index).stringTags();
        System.out.println("tags " + tags);
        tagsList.setItems(FXCollections.observableArrayList(tags));
        tagsList.getSelectionModel().select(0);
    }

    /**
     * This method takes the user back to the album
     * @param actionEvent   ActionEvent button press
     * @throws IOException  Throws IOException
     */
    public void back(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/album.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        albumController controller = loader.getController();
        controller.start(currAlbum, currUser);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * This method creates a new Album
     */
    public void create() {
        Album newAlbum = new Album();

        TextInputDialog dialog = new TextInputDialog("Album Name");
        dialog.setTitle("New Album");
        dialog.setHeaderText("Please enter the name for your new album. No duplicates.");
        dialog.setContentText("Album Name:");


        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {

            if (result.get().equals("") || result.get().contains("/") || result.get().contains("/") || result.get().contains(" ")) {
                functions.invalidAlbName();
                return;
            }

            String albumName = result.get();

            for (Album album : currUser.albums) {
                if (album.getName().equalsIgnoreCase(albumName)) {
                    functions.dupAlbum();
                    return;
                }
            }

            newAlbum = currUser.newAlbum(albumName);

            for (Photo photo : results1) {
                System.out.println("entering");
                File path = new File(photo.getLocation());
                Date d = new Date(path.lastModified());

                Photo picture = new Photo(path.toString(), d);

                if (dupCheck(picture, newAlbum)) {
                    functions.dupPhotos();
                    continue;
                }

                newAlbum.photos.add(picture);
                System.out.println("Photos:" + newAlbum.photos);
            }

        } else {
            functions.incorrectInput();
            return;
        }


    }


    /**
     * Checks to see if there are no duplicates inside of a specific album
     *
     * @param photo photo to check
     * @return true if there is duplicate false if there is no duplciates
     */
    private boolean dupCheck(Photo photo, Album album) {
        for (Photo p : album.photos) {
            if (p.getLocation().equals(photo.getLocation())) {
                return true;
            }
        }
        return false;
    }


}
