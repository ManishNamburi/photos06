package sample.Controllers;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.classes.Admin;
import sample.classes.Album;
import sample.classes.Photo;
import sample.classes.User;

import java.io.File;
import java.io.IOException;
import java.util.Date;


/**
 * Main.java - Main controller for javafx
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */



public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        //Parent root = FXMLLoader.load(getClass().getResource("Login.fxml"));

        try{

            Admin.load();
            initStock();

            Parent root = FXMLLoader.load(getClass().getResource("/sample/views/Login.fxml"));
            primaryStage.setTitle("Photo Album");
            primaryStage.setScene(new Scene(root, 300, 275));
            primaryStage.show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {

        launch(args);
    }



    @Override
    public void stop() throws IOException {
        // executed when the application shuts down
        try{
            System.out.println("testing save");
            Admin.save();
        }
        catch (Exception e){
            e.printStackTrace();
            System.out.println("closing error");
        }
    }

    public void initStock()
    {
        String path0 = "src/sample/data/image0.jpg";
        String path1 = "src/sample/data/image1.jpg";
        String path2 = "src/sample/data/image2.jpg";
        String path3 = "src/sample/data/image3.jpg";
        String path4 = "src/sample/data/image4.jpg";
        String path5 = "src/sample/data/image5.jpg";


        Date d0 = new Date(new File(path0).lastModified());
        Date d1 = new Date(new File(path1).lastModified());
        Date d2 = new Date(new File(path2).lastModified());
        Date d3 = new Date(new File(path3).lastModified());
        Date d4 = new Date(new File(path4).lastModified());
        Date d5 = new Date(new File(path5).lastModified());


        User stock = new User("stock");

        Admin.deleteUser("stock");

        Admin.addUser(stock);

        Album stockAlbum = stock.newAlbum("stock photos");



        Photo newPhoto0 = new Photo(path0, d0);
        Photo newPhoto1 = new Photo(path1, d1);
        Photo newPhoto2 = new Photo(path2, d2);
        Photo newPhoto3 = new Photo(path3, d3);
        Photo newPhoto4 = new Photo(path4, d4);
        Photo newPhoto5 = new Photo(path5, d5);


        stockAlbum.photos.add(newPhoto0);
        stockAlbum.photos.add(newPhoto1);
        stockAlbum.photos.add(newPhoto2);
        stockAlbum.photos.add(newPhoto3);
        stockAlbum.photos.add(newPhoto4);
        stockAlbum.photos.add(newPhoto5);

    }



}
