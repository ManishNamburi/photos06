package sample.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.classes.Admin;
import sample.classes.User;
import sample.classes.functions;

import java.io.IOException;


/**
 * AdminController.java - Holds information for admin.fxml
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */


public class adminController {

    @FXML
    private TextField username;

    @FXML
    ListView<String> currUsers = new ListView<>();


    private ObservableList<String> users;

    /**
     * This method starts the users
     */

    public void start() {
        //currUsers.setPlaceholder(new Label("No rows to display"));\

        users = FXCollections.observableArrayList(Admin.allUsernames());
        currUsers.setItems(users);
        currUsers.getSelectionModel().select(0);

    }

    /**
     * This method gets users
     * @return  users           ArrayList a list of users
     */

    public ObservableList<User> getUsers() {
        ObservableList<User> users = FXCollections.observableArrayList();
        users.add(new User("test user"));
        return users;
    }

    /**
     * This method adds a user
     */

    public void addUser() {

        //currUsers.getItems().add(newUser);

        if(username.getText() == null || username.getText().equals("")){
            functions.invalidUser();
            return;
        }

        if (!Admin.addUser(username.getText())) {
            System.out.println("error");
            functions.invalidUser();
            return;
        }
        else{

            users = FXCollections.observableArrayList(Admin.allUsernames());
            System.out.println(users);
        }

        currUsers.setItems(users);
        currUsers.getSelectionModel().select(users.size()-1);
        currUsers.refresh();
        username.clear();
    }

    /**
     * This method removes a user
     */

    public void removeUser() {
        String u = currUsers.getSelectionModel().getSelectedItem();
//        if (u.equals("stock")) {
//            functions.invalidUserDel();
//            return;
//        }
        Admin.deleteUser(u);
        users.remove(u);
    }

    /**
     * This method logs out a user
     * @param actionEvent           press the button
     * @throws IOException
     */

    public void logoutUser(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/Login.fxml"));
        Parent parent = loader.load();

        //adminPage controller = loader.<adminPage>getController();
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        loginController controller = loader.getController();
        stage.setScene(scene);
        stage.show();
    }


}
