package sample.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import sample.classes.Album;
import sample.classes.Photo;
import sample.classes.User;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;


/**
 * PhotoController.java - Holds information for individual_photo.fxml
 * @author Vishnu DHANASEKARAN vd247
 * @author Manish Namburi msn68
 */


public class photoController {

    @FXML
    private ImageView imageView;

    @FXML
    Label captionLabel, dateLabel;

    @FXML
    private ListView<String> tagsList;

    @FXML
    private TitledPane title;


    public static Album album;
    private Photo photo;
    private User user;

    private static final ObservableList<ImageView> obs = FXCollections.observableArrayList();

    private static final ArrayList<Photo> photoList = new ArrayList<Photo>();

    /**
     * This methods starts the individual photo view and fetches all the data needed
     * @param currPhoto             Photo the current photo selected
     * @param currAlbum             Album the current album the user is in
     * @param currUser              User the user who is viewing the photo
     * @throws IOException          throws IOException if error
     */
    public void start(Photo currPhoto, Album currAlbum, User currUser) throws IOException {
        photoList.clear();
        album = currAlbum;
        photo = currPhoto;
        user = currUser;

        title.setText("["+currAlbum.getName()+"] Photo Viewer");

        File path = new File(album.photos.get(album.photos.indexOf(currPhoto)).getLocation());

        Image image = new Image(path.toURI().toString(), 100, 100, false, false);
        imageView.setImage(image);
        dateLabel.setText(photo.getStringDate());
        captionLabel.setText(photo.getCaption());
        updateTags();
    }

    /**
     * This method takes the user back to the opened Album page
     * @param actionEvent       On button press this method executes
     * @throws IOException      Throws IOException if there is an error
     */
    public void back(javafx.event.ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/sample/views/album.fxml"));
        Parent parent = loader.load();
        Scene scene = new Scene(parent);
        Stage stage = (Stage) ((Node) actionEvent.getSource()).getScene().getWindow();
        albumController controller = loader.getController();
        controller.start(album, user);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * This method changes the photo viewed to the previous photo in the album
     * @param actionEvent   On button click the method executes
     */
    public void leftButton(javafx.event.ActionEvent actionEvent) {
        int index = album.photos.indexOf(photo);
        if(index < 0){
            System.out.println("stopping");
            return;
        }else if(index > 0){
            index-=1;
            photo = album.photos.get(index);
            File path = new File(album.photos.get(index).getLocation());

            Image image = new Image(path.toURI().toString(), 100, 100, false, false);
            imageView.setImage(image);
            dateLabel.setText(photo.getStringDate());
            captionLabel.setText(photo.getCaption());
            updateTags();
        }

    }

    /**
     * This method updates the tags for a photo
     */
    public void updateTags() {
        ArrayList<String> tags = photo.stringTags();
        //System.out.println("tags " + tags);
        tagsList.setItems(FXCollections.observableArrayList(tags));
        tagsList.getSelectionModel().select(0);
    }


    /**
     * This method changes the photo viewed to the next photo in the album
     */
    public void rightButton(){
        int index = album.photos.indexOf(photo);
        if(index >= album.photos.size()-1){
            System.out.println("stopping");
            return;
        }else{
            index += 1;
            photo = album.photos.get(index);
            File path = new File(album.photos.get(index).getLocation());

            Image image = new Image(path.toURI().toString(), 100, 100, false, false);
            imageView.setImage(image);
            dateLabel.setText(photo.getStringDate());
            captionLabel.setText(photo.getCaption());
        }
        updateTags();
    }

}
